import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransportService {

  // URL to web api, here it's a mock
  private url = 'assets/mocks/transport.data.json';

  constructor(private http: HttpClient) { }

  getTransport(): Observable<TransportData> {
    return this.http.get<TransportData>(this.url);
  }
}

export class TransportData {
  'tram1': string;
  'tram2': string;
  'tram3': string;
  'home1': string;
  'home2': string;
  'home3': string;
  'bike': string;
}
