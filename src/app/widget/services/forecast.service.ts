import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ForecastService {

  // URL to web api, here it's a mock
  private url = 'assets/mocks/forecast.data.json';

  constructor(private http: HttpClient) { }

  getForecast(): Observable<ForecastData> {
    return this.http.get<ForecastData>(this.url);
  }
}

export class ForecastData {
  'currentTemp': string;
  'minTemp': string;
  'maxTemp': string;
  'location': string;
}
