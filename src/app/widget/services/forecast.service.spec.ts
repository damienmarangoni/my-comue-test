import { TestBed } from '@angular/core/testing';
import { HttpClientModule }    from '@angular/common/http';

import { ForecastService } from './forecast.service';
import { ForecastComponent } from '../forecast/forecast.component';

describe('ForecastService', () => {
  beforeEach(() =>     TestBed.configureTestingModule({
    declarations: [ ForecastComponent ],
    imports: [
      HttpClientModule]
  }));

  it('should be created', () => {
    const service: ForecastService = TestBed.get(ForecastService);
    expect(service).toBeTruthy();
  });

  it('call of transport service',
  (done: DoneFn) => {    
    const service: ForecastService = TestBed.get(ForecastService);
    service.getForecast().subscribe(value => {
      expect(value.currentTemp).toBe("13°");
      done();
    });    
  });
});
