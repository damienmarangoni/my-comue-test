import { TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';

import { TransportService } from './transport.service';

describe('TransportService', () => {
  beforeEach(() =>  TestBed.configureTestingModule({
    //declarations: [ HomePageComponent, NotificationComponent, ForecastComponent, TransportComponent ],
    imports: [
      HttpClientModule],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
  }));

  it('should be created', () => {
    const service: TransportService = TestBed.get(TransportService);
    expect(service).toBeTruthy();
  });

  it('call of transport service',
  (done: DoneFn) => {    
    const service: TransportService = TestBed.get(TransportService);
    service.getTransport().subscribe(value => {
      expect(value.bike).toBe("23min");
      done();
    });

  });
});
