import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';
import { TransportData, TransportService } from '../services/transport.service';

@Component({
  selector: 'app-transport',
  templateUrl: './transport.component.html',
  styleUrls: ['./transport.component.scss']
})
export class TransportComponent implements OnInit, OnDestroy {

  sub: Subscription;

  // Initialization values
  currentTransport: TransportData = {
    tram1: '12:30',
    tram2: '12:37',
    tram3: '12:45',
    home1: '12:03',
    home2: '12:15',
    home3: '12:21',
    bike: '23min'
};

  constructor(private transportService: TransportService) { }

  ngOnInit() {
    // Receive data from http service
     this.sub = this.transportService.getTransport()
    .subscribe(data => this.currentTransport = data);
  }

  ngOnDestroy() {
    // Don't forget to unsubscribe for memory leaks
    this.sub.unsubscribe();
  }

}
