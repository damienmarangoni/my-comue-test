import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';
import { ForecastData, ForecastService } from '../services/forecast.service';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss']
})
export class ForecastComponent implements OnInit, OnDestroy {

  sub: Subscription;
  
  // Initialization values
  currentForecast: ForecastData = {
    "currentTemp": "",
    "minTemp": "",
    "maxTemp": "",
    "location": ""
};

  constructor(private forecastService: ForecastService) { }

  ngOnInit() {
    // Receive data from http service
     this.sub = this.forecastService.getForecast()
    .subscribe(data => this.currentForecast = data);
  }

  ngOnDestroy() {
    // Don't forget to unsubscribe for memory leaks
    this.sub.unsubscribe();
  }

}
