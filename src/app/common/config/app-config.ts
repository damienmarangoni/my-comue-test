export const NAVIGATIONMENUS = [
    {menu : 'Accueil', class: 'active', link: '/home'},
    {menu : 'Actualités', class: '', link: '/news'},
    {menu : 'Evénements', class: '', link: '/events'},
    {menu : 'Informations', class: '', link: '/infos'}];

export const ALERTMESSAGE = 'Vos résultats d\'examens sont disponibles en ligne';
export const USERMESSAGE = 'Nous vous informons qu\'une opération de maintenace est prévue ce week-end';

export const DIVSTYLEINACTIVE = { margin: 'auto', width: '20%', height: '4em', 'background-color': '', 'border-radius': '0.5em' };
export const DIVSTYLEACTIVE = { margin: 'auto', width: '20%', height: '4em', 'background-color': '#242464', 'border-radius': '0.5em' };

export const ICONSTYLEINACTIVE = {color: '#242464', 'margin-left': '22%'};
export const ICONSTYLEACTIVE = {color: '#FFFFFF', 'margin-left': '22%'};
