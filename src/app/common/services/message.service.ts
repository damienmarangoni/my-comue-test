import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  fireMessageSource: Subject<boolean> = new Subject<boolean>();

  fireMessage$ = this.fireMessageSource.asObservable();

  constructor() { }

  fireMessage(b: boolean) {
    this.fireMessageSource.next(b);
  }
}
