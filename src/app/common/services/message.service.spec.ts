import { TestBed } from '@angular/core/testing';

import { MessageService } from './message.service';

describe('MessageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MessageService = TestBed.get(MessageService);
    expect(service).toBeTruthy();
  });

  it('should be created', () => {
    const service: MessageService = TestBed.get(MessageService);
    service.fireMessage(true);
  });

  it('call of user message service',
  (done: DoneFn) => {    
    const service: MessageService = TestBed.get(MessageService);
    service.fireMessage$.subscribe(value => {
      expect(value).toBe(true);
      done();
    });
    
    service.fireMessage(true);
  });

});
