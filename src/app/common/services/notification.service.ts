import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  fireNotifSource: Subject<boolean> = new Subject<boolean>();

  fireNotif$ = this.fireNotifSource.asObservable();

  constructor() { }

  fireNotif(b: boolean) {
    this.fireNotifSource.next(b);
  }
}
