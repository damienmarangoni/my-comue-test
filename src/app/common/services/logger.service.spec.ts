import { TestBed } from '@angular/core/testing';

import { LoggerService } from './logger.service';

describe('LoggerService', () => {
  let service: LoggerService;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = new LoggerService();

});

  it('should be created', () => {
    expect(service)
    .toBeTruthy();
  });

  it('service.info() should be called', () => {
    service.info("infos");
  });

  it('service.warn() should be called', () => {
    service.warn("warnings");
  });
  it('service.error() should be called', () => {
    service.error("errors");
  });
});
