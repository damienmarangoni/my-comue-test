import { TestBed } from '@angular/core/testing';

import { NotificationService } from './notification.service';

describe('NotificationService', () => {
  let service: NotificationService;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = new NotificationService();

});

  it('should be created', () => {
    const service: NotificationService = TestBed.get(NotificationService);
    expect(service).toBeTruthy();
  });

  it('service.fireAlert() should be called', () => {
    service.fireNotif(true);
  });

  it('call of notification service',
  (done: DoneFn) => {    
    const service: NotificationService = TestBed.get(NotificationService);
    service.fireNotif$.subscribe(value => {
      expect(value).toBe(true);
      done();
    });
    
    service.fireNotif(true);
  });
});
