import { Component, OnInit } from '@angular/core';
import { BottomMenuService } from '../bottom-menu/services/bottom-menu.service';

@Component({
  selector: 'app-dropdown-menu',
  templateUrl: './dropdown-menu.component.html',
  styleUrls: ['./dropdown-menu.component.scss']
})
export class DropdownMenuComponent implements OnInit {

  constructor(private bottomMenuService: BottomMenuService) { }

  ngOnInit() {
  }

  resetBottomMenu(s: string) {
    this.bottomMenuService.resetMenu(s);
  }

}
