import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';

import { Subscription } from 'rxjs';
import { ALERTMESSAGE } from '../../common/config/app-config';
import { NotificationService } from '../../common/services/notification.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit, OnDestroy {

    @ViewChild('notif') notif: ElementRef;

    message: string = ALERTMESSAGE;

    sub: Subscription;

    constructor(private notificationService: NotificationService) { }

    ngOnInit() {
      // Receive observable from app component
      this.sub = this.notificationService.fireNotif$.subscribe(data => { this.fireNotif(); });
    }

    // Display notification
    fireNotif() {
      this.notif.nativeElement.classList.add('show');
    }

    // Close notification
    closeNotif() {
      this.notif.nativeElement.classList.remove('show');
    }

    ngOnDestroy() {
      // Close subscription to avoid memory leaks
      this.sub.unsubscribe();
    }
}
