import { Component, OnInit } from '@angular/core';

import { NAVIGATIONMENUS } from '../../common/config/app-config';
import { LoggerService } from '../../common/services/logger.service';
import { BottomMenuService } from '../bottom-menu/services/bottom-menu.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  showNavbar = false;
  dropdownSet = false;

  // Navigation menus initialization
  customClasses: Array<CustomClasses> = NAVIGATIONMENUS;

  constructor(private logger: LoggerService, private bottomMenuService: BottomMenuService) { }

  ngOnInit() {
  }

  // Toggle menu on burger menu click
  toggleNavbar() {
    this.showNavbar = !this.showNavbar;
  }

  // Display dropdown menu on click
  isClicked() {
    this.dropdownSet = true;
    this.logger.info('Le menu est cliqué!');
  }

  // Correlation between navigation and bottom menu selection
  resetBottomMenu(s: string) {
    this.bottomMenuService.resetMenu(s);
  }

  // Active menu item on click
  activeMenuItem(o: CustomClasses) {
    this.customClasses.forEach(cc => cc.class = '');
    o.class = 'active';
    // Correlation between navigation and bottom menu selection
    this.bottomMenuService.resetMenu(o.link);
  }
}

// Contains menu class
export interface CustomClasses {
  menu: string;
  class: string;
  link: string;
}
