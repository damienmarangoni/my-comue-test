import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { BottomMenuService } from './services/bottom-menu.service';

import { DIVSTYLEACTIVE, DIVSTYLEINACTIVE, ICONSTYLEACTIVE, ICONSTYLEINACTIVE, NAVIGATIONMENUS } from '../../common/config/app-config';

@Component({
  selector: 'app-bottom-menu',
  templateUrl: './bottom-menu.component.html',
  styleUrls: ['./bottom-menu.component.scss']
})
export class BottomMenuComponent implements OnInit, OnDestroy {

  sub: Subscription;

  // Icons background style initialization
  divStyle: Array<Object> = [
    DIVSTYLEINACTIVE,
    DIVSTYLEINACTIVE,
    DIVSTYLEINACTIVE,
    DIVSTYLEINACTIVE,
    DIVSTYLEINACTIVE];

  // Icons style initialization
  iconStyle: Array<Object> =  [
    ICONSTYLEINACTIVE,
    ICONSTYLEINACTIVE,
    ICONSTYLEINACTIVE,
    ICONSTYLEINACTIVE,
    ICONSTYLEINACTIVE];

  constructor(private bottomMenuService: BottomMenuService) { }

  ngOnInit() {
    // Active the chosen menu
    this.sub = this.bottomMenuService.bottomMenuConfig$
    .subscribe(data => {
        this.changeDivStyle(data);
    });
  }

  // Style changes on click
  changeDivStyle(n: number) {
    this.divStyle.forEach((value, index) => {
      if (n === index) {
        this.divStyle[n] = DIVSTYLEACTIVE;
      } else {
        this.divStyle[index] = DIVSTYLEINACTIVE;
      }
    });

    this.iconStyle.forEach((value, index) => {
      if (n === index) {
        this.iconStyle[n] = ICONSTYLEACTIVE;
      } else {
        this.iconStyle[index] = ICONSTYLEINACTIVE;
      }
    });
  }

  ngOnDestroy() {
    // Close subscription to avoid memory leaks
    this.sub.unsubscribe();
  }

}
