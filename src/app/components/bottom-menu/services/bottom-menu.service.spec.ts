import { TestBed } from '@angular/core/testing';

import { BottomMenuService } from './bottom-menu.service';

describe('BottomMenuService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BottomMenuService = TestBed.get(BottomMenuService);
    expect(service).toBeTruthy();
  });

  it('call of notification service',
  (done: DoneFn) => {    
    const service: BottomMenuService = TestBed.get(BottomMenuService);
    service.bottomMenuConfig$.subscribe(value => {
      expect(value).toBe(10);
      done();
    });
    
    service.resetMenu('/home');
  });
});
