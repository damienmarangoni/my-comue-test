import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BottomMenuService {

  n: number;

  bottomMenuConfigSource: Subject<number> = new Subject<number>();

  bottomMenuConfig$ = this.bottomMenuConfigSource.asObservable();

  constructor() { }

  resetMenu(s: string) {

    // Correlation between route and menu
    switch (s) {
      case '/events':
        this.n = 3;
        break;
      case '/infos':
        this.n = 1;
        break;
      case '/main':
          this.n = 2;
          break;
      case '/account':
        this.n = 0;
        break;
      default:
      this.n = 10;
    }

    this.bottomMenuConfigSource.next(this.n);
  }
}
