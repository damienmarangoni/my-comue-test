import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';

import { Subscription } from 'rxjs';
import { USERMESSAGE } from '../../common/config/app-config';
import { MessageService } from '../../common/services/message.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit, OnDestroy {

  @ViewChild('mess') mess: ElementRef;

  message: string = USERMESSAGE;

  sub: Subscription;

  constructor(private messageService: MessageService) { }

  ngOnInit() {
    // Receive observable from app component to display message
    this.sub = this.messageService.fireMessage$.subscribe(data => { this.fireMessage(); });
  }

  // Display message on app command
  fireMessage() {
    this.mess.nativeElement.classList.add('show');
  }

  // Close message on click
  closeMessage() {
    this.mess.nativeElement.classList.remove('show');
  }

  ngOnDestroy() {
    // Close subscription to avoid memory leaks
    this.sub.unsubscribe();
  }
}
