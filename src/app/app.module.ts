import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BottomMenuComponent } from './components/bottom-menu/bottom-menu.component';
import { DropdownMenuComponent } from './components/dropdown-menu/dropdown-menu.component';
import { MessageComponent } from './components/message/message.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { NotificationComponent } from './components/notification/notification.component';
import { GenericPageComponent } from './pages/generic-page/generic-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { MainMenuComponent } from './pages/main-menu-page/main-menu.component';
import { MyAccountComponent } from './pages/my-account-page/my-account.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { ForecastComponent } from './widget/forecast/forecast.component';
import { TransportComponent } from './widget/transport/transport.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    BottomMenuComponent,
    HomePageComponent,
    MainMenuComponent,
    MyAccountComponent,
    DropdownMenuComponent,
    PageNotFoundComponent,
    GenericPageComponent,
    NotificationComponent,
    MessageComponent,
    ForecastComponent,
    TransportComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
