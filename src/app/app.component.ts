import { Component, OnInit } from '@angular/core';
import { MessageService } from './common/services/message.service';
import { NotificationService } from './common/services/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'my-comue-test';

  constructor(private notificationService: NotificationService, private messageService: MessageService) {}

  ngOnInit() {
    // Fire notification in home page through observable
    setTimeout(() => {
         this.notificationService.fireNotif(true);
       }, 10000);
  }
}
