import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GenericPageComponent } from './pages/generic-page/generic-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { MainMenuComponent } from './pages/main-menu-page/main-menu.component';
import { MyAccountComponent } from './pages/my-account-page/my-account.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'main', component: MainMenuComponent },
  { path: 'account', component: MyAccountComponent },
  { path: 'news', component: GenericPageComponent },
  { path: 'events', component: GenericPageComponent },
  { path: 'messages', component: GenericPageComponent },
  { path: 'infos', component: GenericPageComponent },
  { path: 'home',   redirectTo: '', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload', useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
