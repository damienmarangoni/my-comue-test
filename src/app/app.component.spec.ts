import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { BottomMenuComponent } from './components/bottom-menu/bottom-menu.component';
import { DropdownMenuComponent } from "./components/dropdown-menu/dropdown-menu.component";
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { MessageService } from './common/services/message.service';
import { MessageComponent } from './components/message/message.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent, NavigationComponent, BottomMenuComponent, DropdownMenuComponent, MessageComponent
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'my-comue-test'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('my-comue-test');
  });

  it(`should contains user message`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;    
    expect(app).toBeTruthy();

    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('app-message')).not.toBe(null);
  });
});
